insert into brands (id, name, country_iso, years) values
(1, 'Zara', 'ES', 8);

insert into products(id, name, description, stock, brand_id) values
(35455,'Zapatos', 'Cuero de lagarto negro', 100, 1);

insert into prices(id, product_id, brand_id, priority, start_date, end_date, currency, price) values
(1, 35455, 1, 0, '2020-06-14 00.00.00','2020-12-31 23.59.59', 'EUR', 35.50),
(2, 35455, 1, 1, '2020-06-14 15.00.00','2020-06-14 18.30.00', 'EUR', 25.50),
(3, 35455, 1, 1, '2020-06-15 00.00.00','2020-06-15 11.00.00', 'EUR', 30.50),
(4, 35455, 1, 1, '2020-06-15 16.00.00','2020-12-31 23.59.59', 'EUR', 38.95);
