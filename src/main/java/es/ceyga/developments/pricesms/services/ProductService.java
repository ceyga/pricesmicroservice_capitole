package es.ceyga.developments.pricesms.services;

import es.ceyga.developments.pricesms.model.dtos.ProductDTO;

import java.util.Optional;

public interface ProductService {
    
    Optional<ProductDTO> getProductById(Long id);
}
