package es.ceyga.developments.pricesms.dao.repositories;

import es.ceyga.developments.pricesms.dao.entities.BrandEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BrandRepository extends JpaRepository<BrandEntity, Long> {
}
