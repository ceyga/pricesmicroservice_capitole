package es.ceyga.developments.pricesms.mappers;

import es.ceyga.developments.pricesms.dao.entities.PriceEntity;
import es.ceyga.developments.pricesms.dao.entities.ProductEntity;
import es.ceyga.developments.pricesms.model.dtos.PriceDTO;
import es.ceyga.developments.pricesms.model.dtos.ProductDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel="spring")
public interface PriceMapper {
    
    PriceDTO getPriceDTO(PriceEntity priceEntity);
    PriceEntity getPriceEntity(PriceDTO priceDTO);
    List<PriceDTO> getPriceDtos(List<PriceEntity> priceEntities);
    List<PriceEntity> getPriceEntities(List<PriceDTO> priceDTOS);
    
    @Mapping(source = "productEntity.brand.id",target = "brandId")
    @Mapping(source = "productEntity.brand.name",target = "brandName")
    ProductDTO getProductDTO(ProductEntity productEntity);
}
