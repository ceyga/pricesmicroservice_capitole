package es.ceyga.developments.pricesms.mappers;

import es.ceyga.developments.pricesms.dao.entities.ProductEntity;
import es.ceyga.developments.pricesms.model.dtos.ProductDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel="spring")
public interface ProductMapper {
    
    @Mapping(source = "productEntity.brand.id",target = "brandId")
    @Mapping(source = "productEntity.brand.name",target = "brandName")
    ProductDTO getProductDTO(ProductEntity productEntity);
    
    ProductEntity getProductEntity(ProductDTO productDTO);
    
    List<ProductDTO> getProductDtos(List<ProductEntity> productEntities);
    
    List<ProductEntity> getProductEntities(List<ProductDTO> productDTOS);
}
