package es.ceyga.developments.pricesms.model.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.tomcat.jni.Local;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class PriceRequestDTO {
    
    private Long brandId;
    private Long productId;
    private String exactDate;
}
