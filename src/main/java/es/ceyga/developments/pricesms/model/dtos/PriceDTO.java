package es.ceyga.developments.pricesms.model.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import es.ceyga.developments.pricesms.model.enums.CurrencyCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class PriceDTO {
    
    private Long id;
    
    private ProductDTO product;
    
    private BrandDTO brand;
    
    private Integer priority;
    
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startDate;
    
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endDate;
    
    private CurrencyCode currency;
    
    private BigDecimal price;
}
