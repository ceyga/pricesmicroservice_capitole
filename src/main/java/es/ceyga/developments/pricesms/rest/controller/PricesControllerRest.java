package es.ceyga.developments.pricesms.rest.controller;

import es.ceyga.developments.pricesms.dao.entities.BrandEntity;
import es.ceyga.developments.pricesms.model.dtos.BrandDTO;
import es.ceyga.developments.pricesms.model.dtos.PriceDTO;
import es.ceyga.developments.pricesms.model.dtos.PriceRequestDTO;
import es.ceyga.developments.pricesms.model.dtos.ProductDTO;
import es.ceyga.developments.pricesms.model.enums.CurrencyCode;
import es.ceyga.developments.pricesms.services.BrandService;
import es.ceyga.developments.pricesms.services.PriceService;
import es.ceyga.developments.pricesms.services.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.TimeZone;
import java.util.stream.Collectors;

@RestController
@RequestMapping("prices")
public class PricesControllerRest {
    
    private Logger log = LoggerFactory.getLogger(PricesControllerRest.class);
    
    @Autowired
    private BrandService brandService;
    
    @Autowired
    private ProductService productService;
    
    @Autowired
    private PriceService priceService;
    
    @GetMapping("/{id}")
    public ResponseEntity<PriceDTO> getPriceById(@PathVariable("id") Long id){
        log.info("Creando Price");
        PriceDTO priceDTO = new PriceDTO();
        priceDTO.setId(id);
        priceDTO.setCurrency(CurrencyCode.EUR);
        ZoneId utc = ZoneId.of("UTC");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone(utc));
        calendar.set(Calendar.MILLISECOND, 0);
        
        LocalDateTime startDateTime = LocalDateTime.ofInstant(calendar.toInstant(),utc);
        calendar.add(Calendar.HOUR, 10);
        LocalDateTime endDateTime = LocalDateTime.ofInstant(calendar.toInstant(),utc);
    
        priceDTO.setStartDate(startDateTime);
        priceDTO.setEndDate(endDateTime);
    
        Optional<BrandDTO> brandDTO = brandService.getBrandById(1L);
        if (!brandDTO.isEmpty()){
            log.info(brandDTO.get().getName());
        }
        
        Optional<ProductDTO> productDTO = productService.getProductById(35455L);
        if (!productDTO.isEmpty()){
            log.info(productDTO.get().toString());
        }
    
        Optional<PriceDTO> priceDTONew = priceService.getPriceById(2L);
        if (!priceDTONew.isEmpty()){
            log.info(priceDTONew.get().toString());
        }
        
        
        
        String str = "2020-06-14 17:30:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(str, formatter);
        priceService.getPriceByBrandIdAndProductIdAndLocalDateTime(1L, 2L, dateTime);
        Optional<List<Optional<PriceDTO>>> optionalList =
                priceService.getPriceByBrandIdAndProductIdAndLocalDateTime(1L,35455L, dateTime );
    
        List<Optional<PriceDTO>>  optionalList1 = optionalList.get();
    
        List<PriceDTO> filteredList = optionalList1.stream()
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    
        PriceDTO priceMaxPriority = filteredList
                .stream()
                .max(Comparator.comparing(PriceDTO::getPriority))
                .orElseThrow(NoSuchElementException::new);
        
        
        return ResponseEntity.ok(priceMaxPriority);
    }
    
    @PostMapping( consumes = "application/json", produces = "application/json")
    public ResponseEntity<PriceDTO> getByDateAndBrandAndProduct(@RequestBody PriceRequestDTO priceRequestModel){
    
        ResponseEntity<PriceDTO> responseEntity = ResponseEntity.notFound().build();
        String str = priceRequestModel.getExactDate();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(str, formatter);

        Optional<List<Optional<PriceDTO>>> optionalList =
                priceService.getPriceByBrandIdAndProductIdAndLocalDateTime(
                        priceRequestModel.getBrandId(),
                        priceRequestModel.getProductId(),
                        dateTime );
        

        List<Optional<PriceDTO>> optionalList1 = optionalList.get();

        List<PriceDTO> filteredList = optionalList1.stream()
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
        PriceDTO priceMaxPriority;
        
        try{
            priceMaxPriority = filteredList
                    .stream()
                    .max(Comparator.comparing(PriceDTO::getPriority))
                    .orElseThrow(NoSuchElementException::new);
            
            responseEntity = ResponseEntity.ok(priceMaxPriority);
        }catch (NoSuchElementException exception){
            log.error("NOT FOUND PRICES LIST");
        }


        return responseEntity;
        
        
        

    }
    
    
    
}
