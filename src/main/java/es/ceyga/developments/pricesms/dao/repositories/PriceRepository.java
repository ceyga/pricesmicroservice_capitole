package es.ceyga.developments.pricesms.dao.repositories;

import es.ceyga.developments.pricesms.dao.entities.BrandEntity;
import es.ceyga.developments.pricesms.dao.entities.PriceEntity;
import es.ceyga.developments.pricesms.dao.entities.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface PriceRepository extends JpaRepository<PriceEntity, Long> {
    
    @Query(value="select * from prices where brand_id = ?1 and product_id = ?2 and ?3 between START_DATE  and END_DATE ORDER BY PRIORITY DESC" ,nativeQuery = true)
    List<Optional<PriceEntity>> findByBrandAndProductAndDate(Long brandId, Long productId, LocalDateTime localDateTime);
}
