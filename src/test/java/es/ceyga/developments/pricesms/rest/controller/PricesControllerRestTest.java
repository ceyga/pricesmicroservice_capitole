package es.ceyga.developments.pricesms.rest.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import es.ceyga.developments.pricesms.dao.repositories.PriceRepository;
import es.ceyga.developments.pricesms.model.dtos.PriceDTO;
import es.ceyga.developments.pricesms.model.dtos.PriceRequestDTO;
import es.ceyga.developments.pricesms.services.BrandServiceImpl;
import es.ceyga.developments.pricesms.services.PriceServiceImpl;
import es.ceyga.developments.pricesms.services.ProductServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.is;

@DisplayName("Test PriceControllerRest : Controller Layer")
@Slf4j
@SpringBootTest
class PricesControllerRestTest {
    
    @Autowired
    private MockMvc mvc;
    
    private final ObjectMapper objectMapper = new ObjectMapper();

    
    
    @BeforeEach
    void setUp() {
        log.info("Starting Test");
        MockitoAnnotations.initMocks(this);
    }

    @AfterEach
    void tearDown() {
    }
    
    
    @Test
    void getByDateAndBrandAndProduct1() throws Exception {
        //Arrange
        String str = "2020-06-14 17:30:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(str, formatter);
        
        PriceRequestDTO priceRequestDTO = new PriceRequestDTO();
        priceRequestDTO.setBrandId(1L);
        priceRequestDTO.setProductId(35455L);
        priceRequestDTO.setExactDate(str);
    
        String jsonString = objectMapper.writeValueAsString(priceRequestDTO);
    
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post( "/prices")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonString)
                .accept(MediaType.APPLICATION_JSON);
   
        this.mvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id",is(2)))
                .andExpect(jsonPath("$.price",is(25.50)));
        
    }
    
    @Test
    void getByDateAndBrandAndProduct2() throws Exception {
        //Arrange
        String str = "2020-06-14 10:00:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(str, formatter);
        
        PriceRequestDTO priceRequestDTO = new PriceRequestDTO();
        priceRequestDTO.setBrandId(1L);
        priceRequestDTO.setProductId(35455L);
        priceRequestDTO.setExactDate(str);
        
        String jsonString = objectMapper.writeValueAsString(priceRequestDTO);
        
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post( "/prices")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonString)
                .accept(MediaType.APPLICATION_JSON);
        
        this.mvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id",is(1)))
                .andExpect(jsonPath("$.price",is(35.50)));
        
    }
    
    @Test
    void getByDateAndBrandAndProduct3() throws Exception {
        //Arrange
        String str = "2020-06-14 16:00:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(str, formatter);
        
        PriceRequestDTO priceRequestDTO = new PriceRequestDTO();
        priceRequestDTO.setBrandId(1L);
        priceRequestDTO.setProductId(35455L);
        priceRequestDTO.setExactDate(str);
        
        String jsonString = objectMapper.writeValueAsString(priceRequestDTO);
        
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post( "/prices")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonString)
                .accept(MediaType.APPLICATION_JSON);
        
        this.mvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id",is(2)))
                .andExpect(jsonPath("$.price",is(25.50)));
        
    }
    
    @Test
    void getByDateAndBrandAndProduct4() throws Exception {
        //Arrange
        String str = "2020-06-14 21:00:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(str, formatter);
        
        PriceRequestDTO priceRequestDTO = new PriceRequestDTO();
        priceRequestDTO.setBrandId(1L);
        priceRequestDTO.setProductId(35455L);
        priceRequestDTO.setExactDate(str);
        
        String jsonString = objectMapper.writeValueAsString(priceRequestDTO);
        
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post( "/prices")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonString)
                .accept(MediaType.APPLICATION_JSON);
        
        this.mvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id",is(1)))
                .andExpect(jsonPath("$.price",is(35.50)));
        
    }
    
    @Test
    void getByDateAndBrandAndProduct5() throws Exception {
        //Arrange
        String str = "2020-06-15 10:00:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(str, formatter);
        
        PriceRequestDTO priceRequestDTO = new PriceRequestDTO();
        priceRequestDTO.setBrandId(1L);
        priceRequestDTO.setProductId(35455L);
        priceRequestDTO.setExactDate(str);
        
        String jsonString = objectMapper.writeValueAsString(priceRequestDTO);
        
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post( "/prices")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonString)
                .accept(MediaType.APPLICATION_JSON);
        
        this.mvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id",is(3)))
                .andExpect(jsonPath("$.price",is(30.50)));
        
    }
    
    @Test
    void getByDateAndBrandAndProduct6() throws Exception {
        //Arrange
        String str = "2020-06-16 21:00:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(str, formatter);
        
        PriceRequestDTO priceRequestDTO = new PriceRequestDTO();
        priceRequestDTO.setBrandId(1L);
        priceRequestDTO.setProductId(35455L);
        priceRequestDTO.setExactDate(str);
        
        String jsonString = objectMapper.writeValueAsString(priceRequestDTO);
        
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post( "/prices")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonString)
                .accept(MediaType.APPLICATION_JSON);
        
        this.mvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id",is(4)))
                .andExpect(jsonPath("$.price",is(38.95)));
        
    }
    
    @Test
    void getByDateAndBrandAndProductNotFound() throws Exception {
        //Arrange
        String str = "2021-06-16 21:00:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(str, formatter);
        
        PriceRequestDTO priceRequestDTO = new PriceRequestDTO();
        priceRequestDTO.setBrandId(1L);
        priceRequestDTO.setProductId(35455L);
        priceRequestDTO.setExactDate(str);
        
        String jsonString = objectMapper.writeValueAsString(priceRequestDTO);
        
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post( "/prices")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonString)
                .accept(MediaType.APPLICATION_JSON);
        
        this.mvc.perform(requestBuilder)
                .andExpect(status().isNotFound());
        
    }
    
}