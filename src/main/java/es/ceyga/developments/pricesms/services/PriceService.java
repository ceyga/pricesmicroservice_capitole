package es.ceyga.developments.pricesms.services;

import es.ceyga.developments.pricesms.model.dtos.PriceDTO;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface PriceService {
    
    Optional<PriceDTO> getPriceById(Long id);
    Optional<List<Optional<PriceDTO>>> getPriceByBrandIdAndProductIdAndLocalDateTime(Long brandId, Long productId, LocalDateTime localDateTime);
}
