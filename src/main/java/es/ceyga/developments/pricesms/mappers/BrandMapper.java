package es.ceyga.developments.pricesms.mappers;

import es.ceyga.developments.pricesms.dao.entities.BrandEntity;
import es.ceyga.developments.pricesms.model.dtos.BrandDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel="spring")
public interface BrandMapper {
    
    BrandDTO getBrandDTO(BrandEntity brandEntity);
    BrandEntity getBrandEntity(BrandDTO brandDTO);
    List<BrandDTO> getBrandDtos(List<BrandEntity> brandEntities);
    List<BrandEntity> getBrandEntities(List<BrandDTO> brandDTOS);
}
