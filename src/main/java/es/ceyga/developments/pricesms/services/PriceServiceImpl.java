package es.ceyga.developments.pricesms.services;

import es.ceyga.developments.pricesms.dao.entities.PriceEntity;
import es.ceyga.developments.pricesms.dao.repositories.PriceRepository;
import es.ceyga.developments.pricesms.mappers.PriceMapper;
import es.ceyga.developments.pricesms.model.dtos.PriceDTO;
import es.ceyga.developments.pricesms.rest.controller.PricesControllerRest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PriceServiceImpl implements PriceService{
    
    private Logger log = LoggerFactory.getLogger(PriceServiceImpl.class);
    
    @Autowired
    PriceRepository priceRepository;
    
    @Autowired
    PriceMapper priceMapper;
    
    
    @Override
    public Optional<PriceDTO> getPriceById(Long id) {
        Optional<PriceEntity> priceEntityOptional = priceRepository.findById(id);
        PriceDTO priceDTO = priceMapper.getPriceDTO(priceEntityOptional.get());
        return Optional.ofNullable(priceDTO);
    }
    
    
    
    @Override
    public Optional<List<Optional<PriceDTO>>> getPriceByBrandIdAndProductIdAndLocalDateTime(Long brandId, Long productId, LocalDateTime localDateTime) {
        List<Optional<PriceEntity>> priceEntityOptionalList = priceRepository.findByBrandAndProductAndDate(brandId,productId,localDateTime);
        List<Optional<PriceDTO>> listOptionalPriceDTO = new ArrayList<>();
        for (Optional<PriceEntity> priceEntity: priceEntityOptionalList) {
            PriceDTO priceDTO = priceMapper.getPriceDTO(priceEntity.get());
            listOptionalPriceDTO.add(Optional.ofNullable(priceDTO));
        }
        return Optional.ofNullable(listOptionalPriceDTO);
    }
}
