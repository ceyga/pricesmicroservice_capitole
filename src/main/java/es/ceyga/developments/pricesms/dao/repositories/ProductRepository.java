package es.ceyga.developments.pricesms.dao.repositories;

import es.ceyga.developments.pricesms.dao.entities.BrandEntity;
import es.ceyga.developments.pricesms.dao.entities.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<ProductEntity, Long> {

}
