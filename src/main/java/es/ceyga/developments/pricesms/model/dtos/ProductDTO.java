package es.ceyga.developments.pricesms.model.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Data
public class ProductDTO {
    
    private Long id;
    
    @NonNull
    private String name;
    
    private String description;
    
    @NonNull
    private Integer stock;
    
    private Long brandId;
    
    private String brandName;
}
