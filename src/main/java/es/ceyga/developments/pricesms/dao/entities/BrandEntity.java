package es.ceyga.developments.pricesms.dao.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;


@ToString
@NoArgsConstructor
@RequiredArgsConstructor
@Data
@Entity(name = "brands")
public class BrandEntity{
    
    @NonNull
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    
    private String name;
    
    @Column(name = "country_iso")
    private String countryIso;
    
    private Integer years;
    
    @OneToMany(cascade = CascadeType.ALL , fetch = FetchType.EAGER, mappedBy = "brand")
    private List<ProductEntity> products = new ArrayList<ProductEntity>();

}
