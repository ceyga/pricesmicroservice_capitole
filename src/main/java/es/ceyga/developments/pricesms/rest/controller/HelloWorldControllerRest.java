package es.ceyga.developments.pricesms.rest.controller;

import es.ceyga.developments.pricesms.config.ApplicationConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

@RestController
public class HelloWorldControllerRest {
    
    private Logger log = LoggerFactory.getLogger(HelloWorldControllerRest.class);
    
    @Autowired
    private ApplicationConfig applicationConfig;
    
    @GetMapping("/helloWorld")
    public String greeting(){
    
        log.info(applicationConfig.toString());
        
        String[] locales = Locale.getISOCountries();
        
        for (String countryCode : locales) {
        
            Locale obj = new Locale("", countryCode);
        
            log.info("Country Code = " + obj.getCountry()
                    + ", Country Name = " + obj.getDisplayCountry());
        
        }
        return "Hello world for Rest API in Prices Microservices";
    }
}
