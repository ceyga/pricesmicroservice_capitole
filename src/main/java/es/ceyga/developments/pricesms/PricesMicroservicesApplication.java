package es.ceyga.developments.pricesms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PricesMicroservicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(PricesMicroservicesApplication.class, args);
	}

}
