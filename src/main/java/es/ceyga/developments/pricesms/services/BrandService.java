package es.ceyga.developments.pricesms.services;

import es.ceyga.developments.pricesms.model.dtos.BrandDTO;

import java.util.List;
import java.util.Optional;

public interface BrandService {
    
    Optional<BrandDTO> getBrandById(Long id);
    List<BrandDTO> listAllBrands();
    BrandDTO saveBrand(BrandDTO brandDTO);
    void deleteById(Long id);
}
