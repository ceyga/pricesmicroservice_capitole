DROP TABLE IF EXISTS prices;
DROP TABLE IF EXISTS products;
DROP TABLE IF EXISTS brands;

CREATE TABLE brands(
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    country_iso VARCHAR(10) NOT NULL,
    years INT DEFAULT NULL
);

CREATE TABLE products(
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    description VARCHAR(100) NOT NULL,
    stock INT DEFAULT 0,
    brand_id INT,
    FOREIGN KEY (brand_id) REFERENCES brands(id)
);

CREATE TABLE prices(
    id INT AUTO_INCREMENT PRIMARY KEY,
    product_id INT,
    brand_id INT,
    priority INT,
    start_date TIMESTAMP,
    end_date TIMESTAMP,
    currency VARCHAR(10),
    price DECIMAL(10,2),
    FOREIGN KEY (product_id) REFERENCES products(id),
    FOREIGN KEY (brand_id) REFERENCES brands(id)
);