package es.ceyga.developments.pricesms.model.dtos;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.List;

@RequiredArgsConstructor
@NoArgsConstructor
@Data
@ToString
public class BrandDTO {
    
    private Long id;

    @NonNull
    private String name;
    
    @NonNull
    private String countryIso;
    
    @NonNull
    private Integer years;
    
    
}
