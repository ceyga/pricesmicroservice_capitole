package es.ceyga.developments.pricesms.services;

import es.ceyga.developments.pricesms.dao.entities.BrandEntity;
import es.ceyga.developments.pricesms.dao.repositories.BrandRepository;
import es.ceyga.developments.pricesms.mappers.BrandMapper;
import es.ceyga.developments.pricesms.model.dtos.BrandDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BrandServiceImpl implements BrandService{
    
    @Autowired
    BrandRepository brandRepository;
    
    @Autowired
    BrandMapper brandMapper;
    
    
    @Override
    public Optional<BrandDTO> getBrandById(Long id) {
        Optional<BrandEntity> optionalBrandEntity = brandRepository.findById(id);
        BrandEntity brandEntity = optionalBrandEntity.get();
        BrandDTO brandDTO = brandMapper.getBrandDTO(brandEntity);
        return Optional.ofNullable(brandDTO);
        //return Optional.empty();
    }
    
    @Override
    public List<BrandDTO> listAllBrands() {
        return null;
    }
    
    @Override
    public BrandDTO saveBrand(BrandDTO brandDTO) {
        return null;
    }
    
    @Override
    public void deleteById(Long id) {
    
    }
}
