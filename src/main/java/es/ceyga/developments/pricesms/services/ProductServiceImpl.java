package es.ceyga.developments.pricesms.services;

import es.ceyga.developments.pricesms.dao.entities.ProductEntity;
import es.ceyga.developments.pricesms.dao.repositories.ProductRepository;
import es.ceyga.developments.pricesms.mappers.ProductMapper;
import es.ceyga.developments.pricesms.model.dtos.ProductDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProductServiceImpl implements  ProductService{
    
    @Autowired
    ProductRepository productRepository;
    
    @Autowired
    ProductMapper productMapper;
    
    
    @Override
    public Optional<ProductDTO> getProductById(Long id) {
        Optional<ProductEntity> optionalProductEntity = productRepository.findById(id);
        ProductEntity productEntity = optionalProductEntity.get();
        ProductDTO productDTO = productMapper.getProductDTO(productEntity);
        return Optional.ofNullable(productDTO);
    }
}
